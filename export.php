<?php
// Damit alle Fehler angezeigt werden
error_reporting(E_ALL & ~E_NOTICE);
// Eingaben
$ip = "192.168.178.1";
$username = "smarthome_user";
$password = "smarthome_pw";

// Geräte
$device_one = "device_id";

// Werte auslesen
$date = time();
$curTemp = "gettemperature"; // curent temperature

// Daten erfassen

echo getValue($ip, getSid($password, $username, $ip), $device_one, $curTemp);

// Werte in DB schreiben
// Zum Aufbau der Verbindung zur Datenbank
define ( 'MYSQL_HOST_Z','127.0.0.1' );
define ( 'MYSQL_BENUTZER_Z',  'db_user' );
define ( 'MYSQL_KENNWORT_Z',  'db_password' );
define ( 'MYSQL_DATENBANK_Z', 'database' );

//Datenbank verbinden
$db_link_z = ($GLOBALS["___mysqli_ston_z"] = mysqli_connect(MYSQL_HOST_Z,  MYSQL_BENUTZER_Z,  MYSQL_KENNWORT_Z));

//Daten aus DB holen
$db_sel_z = ((bool)mysqli_query($GLOBALS["___mysqli_ston_z"], "USE " . constant('MYSQL_DATENBANK_Z'))) or die("Auswahl der Zieldatenbank fehlgeschlagen");

// in neue Tabelle schreiben
$sql_z = 'INSERT IGNORE INTO
			fb(date, curTemp)
			VALUES ("'.$date.'", "' . getValue($ip, getSid($password, $username, $ip), $device_one, $curTemp) . '")';

$eintragen_z = mysqli_query($GLOBALS["___mysqli_ston_z"], $sql_z) OR die ('Fehler: ' . ((is_object($GLOBALS["___mysqli_ston_z"])) ? mysqli_error($GLOBALS["___mysqli_ston_z"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)) . 'Fehler');


// Funktionen
function getSid($password, $username, $ip)
{
	try {
		$xml = simplexml_load_string(file_get_contents(sprintf('http://%s/login_sid.lua', $ip)));
		$challenge = $xml->Challenge;
	} catch (\Exception $e) {
		throw new BoxNotFoundException();
	}
	$challenge_str = sprintf("%s-%s", $challenge, $password);
	$md5_str = md5(iconv("UTF-8", "UTF-16LE", $challenge_str));
	$xml = simplexml_load_string(file_get_contents(sprintf('http://%s/login_sid.lua?user=%s&response=%s', $ip, $username, $challenge . '-' . $md5_str)));
	$sid = (string)$xml->SID;
	if ($sid == '0000000000000000') {
		throw new InvalidCredentialsException();
	}
	return $sid;
}

function getValue($ip, $sid, $device, $value)
{
	$url = "http://".$ip."/webservices/homeautoswitch.lua?sid=".$sid."&ain=".$device."&switchcmd=".$value;
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	curl_close($curl);
	return $result;
}
?>
